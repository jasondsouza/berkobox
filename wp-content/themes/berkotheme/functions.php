<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);



/*
Update title based on Firstmane of customer
 */

function berko_update_postdata( $value, $post_id, $field ) {
  
  $full_name = get_field('full_name', $post_id);
    $title = $full_name;

  $slug = sanitize_title( $title );
  
  $postdata = array(
       'ID'    => $post_id,
       'post_title'  => $title,
       'post_type'   => 'customer',
       'post_name'   => $slug
    );
  
  wp_update_post( $postdata );
  
  return $value;
  
}
add_filter('acf/update_value/name=full_name', 'berko_update_postdata', 1000, 3);

/*
Login Screen Overrides
 */

function berko_loginURL() {
    return get_site_url();;
}
add_filter('login_headerurl', 'berko_loginURL');

function berko_loginURLtext() {
    return 'Berkowitz Furniture';
}
add_filter('login_headertitle', 'berko_loginURLtext');

function berko_logincustomCSSfile() {
    
    wp_enqueue_style('main', get_template_directory_uri() . '/dist/styles/main.css');
    wp_enqueue_style('login-styles', get_template_directory_uri() . '/login/login_styles.css');
}
add_action('login_enqueue_scripts', 'berko_logincustomCSSfile');

/**
   * Register a customer post type, with REST API support
   *
   * Based on example at: http://codex.wordpress.org/Function_Reference/register_post_type
   */
  add_action( 'init', 'berkof_customer_cpt');
  function berkof_customer_cpt() {
    $labels = array(
        'name'               => _x( 'Customers', 'post type general name', 'berkotheme' ),
        'singular_name'      => _x( 'Customer', 'post type singular name', 'berkotheme' ),
        'menu_name'          => _x( 'Customers', 'admin menu', 'berkotheme' ),
        'name_admin_bar'     => _x( 'Customer', 'add new on admin bar', 'berkotheme' ),
        'add_new'            => _x( 'Add New', 'customer', 'berkotheme' ),
        'add_new_item'       => __( 'Add New Customer', 'berkotheme' ),
        'new_item'           => __( 'New Customer', 'berkotheme' ),
        'edit_item'          => __( 'Edit Customer', 'berkotheme' ),
        'view_item'          => __( 'View Customer', 'berkotheme' ),
        'all_items'          => __( 'All Customers', 'berkotheme' ),
        'search_items'       => __( 'Search Customers', 'berkotheme' ),
        'parent_item_colon'  => __( 'Parent Customers:', 'berkotheme' ),
        'not_found'          => __( 'No customers found.', 'berkotheme' ),
        'not_found_in_trash' => __( 'No customers found in Trash.', 'berkotheme' )
    );

    $args = array(
        'labels'             => $labels,
        'description'        => __( 'Description.', 'berkotheme' ),
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'customer' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'show_in_rest'       => true,
        // 'rest_base'          => 'customers-api',
        'rest_controller_class' => 'WP_REST_Posts_Controller',
        'supports'           => array( 'title', 'author' )
    );

    register_post_type( 'customer', $args );
}

function wp_rest_api_alter() {

  // register_api_field( 'customer',
  //     'customer_details',
  //     array(
  //       'get_callback'    => function($data, $field, $request, $type){
  //         if (function_exists('get_fields')) {
  //           return get_fields($data['id']);
  //         }
  //         return [];
  //       },
  //       'update_callback' => null,
  //       'schema'          => null,
  //     )
  // );
  register_api_field( 'customer',
      'full_name',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'phone_number',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'email_address',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'address',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'suburb',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'state',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'postcode',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'product_of_interest',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'price_quoted',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'company',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
  register_api_field( 'customer',
      'notes',
      array(
          'get_callback'    => 'berkof_get_customer',
          'update_callback' => 'berkof_update_customer',
          'schema'          => null,
      )
  );
}

function berkof_update_customer( $value, $object, $field_name ) {
    if ( ! $value || ! is_string( $value ) ) {
        return;
    }
    return update_post_meta( $object->ID, $field_name, strip_tags( $value ) );
}

function berkof_get_customer( $object, $field_name, $request ) {
    return get_post_meta( $object[ 'id' ], $field_name, true );
}
add_action( 'rest_api_init', 'wp_rest_api_alter');
