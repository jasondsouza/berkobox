berkofsapp.controller('CustomerController',['CustomerService','$scope','$stateParams',function(CustomerService, $scope, $stateParams){
		
	$scope.states = [
	{id:'VIC',name:'Victoria'},
	{id:'NSW',name:'New South Wales'},
	{id:'QLD',name:'Queensland'},
	{id:'SA',name:'South Australia'},
	{id:'WA',name:'Western Austrlia'},
	{id:'TAS',name:'Tasmania'},
	{id:'ACT',name:'ACT'},
	{id:'NT',name:'Northern Territory'}];

	$scope.formdata = {
		full_name : "",
		email_address : "",
		phone_number : "",
		address : "",
		suburb : "",
		state : "",
		postcode : "",
		notes : "",
		price_quoted : "",
		product_of_interest : "",
		company : ""
	};

	CustomerService.getCustomers().success(function(data){
		$scope.customers = data;
		// console.log($scope.customers);
	}).error(function(data){
		$scope.customers = data;
		// console.log($scope.customers);
	});
	

	CustomerService.getCustomer($stateParams.customerid).success(function(data){
		$scope.customer = data;
	}).error(function(data){
		
	});

	CustomerService.saveCustomer($scope.newCustomer).success(function(data){
		$scope.custnew = data;
		console.log($scope.custnew);
	}).error(function(data){
		
	});
	
}]);