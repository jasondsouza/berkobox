berkofsapp.controller('DirectoryController',['$scope','GapiService','$stateParams',function($scope, GapiService, $stateParams){
	
	GapiService.getRootFolders().then(function(data){
		$scope.rootFolders = data;
		console.log('Settign rootFolders');
		console.log($scope.rootFolders);
	},function(data){
		$scope.rootFolders = data;
		console.log($scope.rootFolders);
	});

	GapiService.getChildItems($stateParams.folderid).then(function(data){
		$scope.files = data;
	},function(data){
		$scope.files = data;
	});
	
	$scope.login = function () {
        GapiService.init().then(function (data) {
            $scope.showLogin = false;            
            $scope.showFolders = true;
            
        }, function (err) {
            $scope.data = {};
            $scope.showLogin = true;
            $scope.showFolders = false;
        });
    };

}]);