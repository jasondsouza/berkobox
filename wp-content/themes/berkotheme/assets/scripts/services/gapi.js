berkofsapp.service('GapiService',['GoogleApp','$q','$cacheFactory',function(GoogleApp,$q,$cacheFactory){
	var folderCache = $cacheFactory('folderCache');
	var fileCache = $cacheFactory('fileCache');
	var rfCache = $cacheFactory('rfCache');

	var deferObject;

    var dirMethods = {
    	init: function() {
        var app = GoogleApp,
            deferred = $q.defer(),
            attemptCounter = 0,
            onAuth = function(response) {
                attemptCounter++;
                if (attemptCounter > 3) {
                    deferred.reject('Login attempt failed. Attempted to login ' + attemptCounter + ' times.');
                    return;
                }
                // The response could tell us the user is not logged in.
                if (response && !response.error) {
                    if (response.status && response.status.signed_in === true) {
                        app.oauthToken = gapi.auth.getToken();
                        deferred.resolve(app);
                    } else {
                        deferred.reject("App failed to log-in to Google API services.");
                    }
                } else {
                    deferred.notify('Login attempt failed. Trying again. Attempt #' + attemptCounter);
                    gapi.auth.authorize({
                        client_id: app.clientId,
                        scope: app.scopes,
                        immediate: true
                    }, onAuth);
                }
            };

        deferred.notify('Trying to log-in to Google API services.');

	    function checkGAPILoad() {
		    if(null == gapi.client) {
		      console.log("checking GAPI Load");
		      window.setTimeout(checkGAPILoad,1000);
		      return;
		    }
		    gapi.auth.authorize({
	            client_id: app.clientId,
	            scope: app.scopes,
	            immediate: true
	        }, onAuth);
		  }

		  checkGAPILoad();
        

        return deferred.promise;
    },
    	getRootFolders: function(){
    	var rfdeferObject =  rfdeferObject || $q.defer();
    	this.init().then(function(){
    		
    		gapi.client.load('drive', 'v2', function () {
    			var request = gapi.client.drive.files.list({
                        'q': "mimeType = 'application/vnd.google-apps.folder' and 'root' in parents"
                      });

                request.execute(function (resp) {
                	// console.log(resp);
                	var rfolders = resp.items;
                	// console.log(rfolders);
                   rfdeferObject.resolve(rfolders);
                });
    		});
            },function(){
            	rfdeferObject.reject('error');
            });
			return rfdeferObject.promise;
    	},
    	getChildItems: function(fid){

    	var cideferObject =  cideferObject || $q.defer();
    	var fileid =  fid || '0';
    	this.init().then(function(){
    		
    		gapi.client.load('drive', 'v2', function () {
    			var request = gapi.client.drive.children.list({
                        'folderId': fileid
                      });
                request.execute(function (resp) {
                	// console.log(resp);
                	var childItems = [];
                	for (var i = 0; i < resp.items.length; i++) {
                		childItems.push(fileCache.get(resp.items[i].id));
                	}
                	// console.log('Child Items');
                	// console.log(childItems);
                   cideferObject.resolve(childItems);
                });
    		});
            },function(){
            	cideferObject.reject('error');
            });
			return cideferObject.promise;
    	},
    	getFiles: function() {
    	var fideferObject =  fideferObject || $q.defer();
    	this.init().then(function(){
    		gapi.client.load('drive', 'v2', function () {
    			var request = gapi.client.drive.files.list({
                        'maxResults': 9999
                      });
                request.execute(function (resp) {
                	// console.log(resp);
                	for (var i = 0; i < resp.items.length; i++) {
                		fileCache.put(resp.items[i].id,resp.items[i]);
                	}
                   fideferObject.resolve(resp.items);
                });
    		});
            },function(){
            	fideferObject.reject('error');
            });
    		return fideferObject.promise;
    	},
    	getFolders: function() {
    	var fdeferObject =  fdeferObject || $q.defer();
    	this.init().then(function(){
    		
    		gapi.client.load('drive', 'v2', function () {
    			var request = gapi.client.drive.files.list({
                        'maxResults': 9999,
            			'q': "mimeType = 'application/vnd.google-apps.folder'"
                      });
                request.execute(function (resp) {
                	// console.log(resp);
                   fdeferObject.resolve(resp.items);
                });
    		});
            },function(){
            	fdeferObject.reject('error');
            });
    		return fdeferObject.promise;
    	}
    };

    return dirMethods;
}]);