berkofsapp.service('GDriveService',['$http', '$rootScope', '$q', function ($http, $rootScope, $q) {
            var clientId = '634868256112-boi2m5u06jqbp27undv2a57t73r1jpq3.apps.googleusercontent.com';
                // apiKey = '{API_KEY}',
            var scopes = 'https://www.googleapis.com/auth/drive.readonly';
            var appFolderID = '0B5lCI-7SZGzCYWJWajRYOUJGYU0';
            var logindeferred = $q.defer();
            var querydeferred = $q.defer();
            var statusdeferred = $q.defer();

            this.login = function () {
                gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, this.handleAuthResult);
                return logindeferred.promise;
            };

            this.handleClientLoad = function () {
                //gapi.client.setApiKey(apiKey);
                gapi.auth.init(function () { });
                window.setTimeout(checkAuth, 1);
            };

            this.checkAuthStatus = function () {
                //gapi.client.setApiKey(apiKey);
                gapi.auth.init(function () { });
                window.setTimeout(isAuth, 1);
            };

            this.isAuth = function() {
                gapi.auth.authorize({ 
                    client_id: clientId, 
                    scope: scopes, 
                    immediate: true
                }, function(response){
                    if(response && !response.error) {
                        if(response.status && response.status.signed_in === true) {
                            app.oauthToken = gapi.auth.getToken();
                            statusdeferred.resolve(app);
                        }else{
                            statusdeferred.reject("Failed to log-in");
                        }
                    }
                    return statusdeferred.promise;
                });
            };
            
            this.checkAuth = function() {
                gapi.auth.authorize({ 
                    client_id: clientId, 
                    scope: scopes, 
                    immediate: true
                }, this.handleAuthResult);
            };

            this.isAuthorised = function(){
                gapi.auth.authorize({ 
                    client_id: clientId, 
                    scope: scopes, 
                    immediate: true
                }, this.getAssets);
            };

            this.getAssets = function(authResult){
                if (authResult && !authResult.error) {
                    $rootScope.showLogin = false;
                    var data = {};
                    gapi.client.load('drive', 'v2', function () {
                    var request = gapi.client.drive.files.list({
                        'maxResults': 9999,
                        'q': "mimeType = 'application/vnd.google-apps.folder' and '"+appFolderID+"' in parents"
                      });
                    request.execute(function (resp) {
                       data.files = resp.items;
                       });
                        });
                        logindeferred.resolve(data);
                    } else {
                        logindeferred.reject('error');
                    }
            };

            this.handleAuthResult = function(authResult) {
                if (authResult && !authResult.error) {
                	$rootScope.showLogin = false;
                    var data = {};
                    data.rootFolders = [];
                    data.files = [];
                    gapi.client.load('drive', 'v2', function () {
                        var request = gapi.client.drive.files.list({
				            'maxResults': 9999,
                            // 'q': "mimeType = 'application/vnd.google-apps.folder'"
				            // 'q': "'0B5lCI-7SZGzCS0VHRk51R3FJQU0' in parents"
				          });
                        request.execute(function (resp) {
                           data.allfiles = resp.items;
                           // console.log(data.allfiles);
				            if (data.allfiles && data.allfiles.length > 0) {
				              for (var i = 0; i < data.allfiles.length; i++) {
                                var file = data.allfiles[i];
				                // console.log(file);
                                if(file.mimeType === 'application/vnd.google-apps.folder' && file.parents.isRoot){
                                    data.rootFolders.push(file);
                                }
                                else{
                                    data.files.push(file);                                    
                                }

				              }
				            }
                        });
                    });
                    logindeferred.resolve(data);
                } else {
                    logindeferred.reject('error');
                }
            };

            this.handleAuthClick = function(event) {
                gapi.auth.authorize({ 
                    client_id: clientId, 
                    scope: scopes, 
                    immediate: false
                }, this.handleAuthResult);
                return false;
            };

        }]);