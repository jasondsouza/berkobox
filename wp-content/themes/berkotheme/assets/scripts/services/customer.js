berkofsapp.factory('CustomerService',['$http',function($http){
	
	// return $resource(
	// 	berkofs.api + '/customer/:id',
	// 	{
	// 		nonce: berkofs.nonce,
	// 		author:berkofs.user_id
	// 	});
	// var authorFilter = (((int) berkofs.user_id) === 1) ? '' : '?filter[author]='+berkofs.user_id;
	// 
	
	var authorFilter = '?per_page=9999&filter[author]='+berkofs.user_id;
	if(parseInt(berkofs.user_id) === 1){ authorFilter = '?per_page=9999';}

	return {
    getCustomer : function(cid){
      return $http.get(berkofs.api + '/customer/'+cid)
      .success(function(data) {
           return data;
         })
         .error(function(data) {
           return data;
         });
    },
    getCustomers : function(){
      return $http.get(berkofs.api + '/customer'+authorFilter,{cache:true})
      .success(function(data) {
           return data;
         })
         .error(function(data) {
           return data;
         });
    },
    saveCustomer : function(data){
    	return $http.post(berkofs.api + '/customer', 
    		data, {headers: {'X-WP-Nonce': berkofs.nonce, '_wp_json_nonce': berkofs.nonce}})
      .success(function(data) {
           return data;
         })
         .error(function(data) {
           return data;
         });
    }
  };
}]);