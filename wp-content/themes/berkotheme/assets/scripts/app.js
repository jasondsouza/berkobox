'use strict';
/**
*  Module
*  Berkowitz File System App
* 
*/
var berkofsapp = angular.module('berkofsapp', [
	'ui.router',
	'ngRoute',
	'ngResource'
]);

berkofsapp.value('GoogleApp', {
  appFolderID: '0B5lCI-7SZGzCYWJWajRYOUJGYU0',
  clientId: '634868256112-boi2m5u06jqbp27undv2a57t73r1jpq3.apps.googleusercontent.com',
  scopes: [
    'https://www.googleapis.com/auth/drive.readonly'
  ]
});

// function init() {
//   console.log("init run");
//   window.initGapiService();
// }
berkofsapp.run(['CustomerService','GapiService', '$rootScope','$log', function (CustomerService, GapiService, $rootScope, $log){
	  
  $rootScope.showLogin = true;
   

  GapiService.init().then(function(data){
    $rootScope.showLogin = false;
    GapiService.getFiles().then(function(data){
      $rootScope.showFolders = true; 
    });
  },function(err){
    $rootScope.showLogin = true;
    $log.error('failed to authenticate',err);
  });
	
}]);

berkofsapp.config(['$stateProvider', '$locationProvider', '$urlRouterProvider',function ($stateProvider, $locationProvider, $urlRouterProvider) {//   var currentUser = wpAPIResource.get( {
//     param1: 'users',
//     param2: berkofs.user_id
// } );
  
  $stateProvider
    .state('home', {
      url: '/',
      views: {        
        'rootfolders' : {
          templateUrl: ''+ berkofs.ngtemplates +'/directorylist.html'
        }
      }      
    })
    .state('home.directory', {
      url: 'directory/:folderid',
      views: {
        'utility' : {
          templateUrl: ''+ berkofs.ngtemplates +'/filesort.html',
          controller: 'DirectoryController'
        },
        'gdrivecontent@' : {
          templateUrl: ''+ berkofs.ngtemplates +'/filelist.html',
          controller: 'DirectoryController'
        },
        'customercontent' : {}
      }
    })
    .state('customer', {
      url: '/customer',
      views: {
        'rootfolders' : {
          templateUrl: ''+ berkofs.ngtemplates +'/directorylist.html'
        },
        'uitility' : {
          // templateUrl: ''+ berkofs.ngtemplates +'/concerts.html',
          // controller: 'ConcertsController'
        },
        'gdrivecontent@customer' : {},
        'customercontent' : {
          templateUrl: ''+ berkofs.ngtemplates +'/customermenu.html',
          // controller: function($scope, $stateParams){}
        }
      }
    })
    .state('customer.new', {
      url: '/customer/new',
      views: {
        'rootfolders' : {
          templateUrl: ''+ berkofs.ngtemplates +'/directorylist.html'
        },
        'utility' : {},
        'gdrivecontent@customer.new' : {},
        'customercontent@' : {
        	templateUrl: ''+ berkofs.ngtemplates +'/customerform.html',
        	controller: 'CustomerController'
        }
        
      }
    })
    .state('customer.list', {
      url: '/customer/list',
      views: {
        'rootfolders' : {
          templateUrl: ''+ berkofs.ngtemplates +'/directorylist.html'
        },
        'utility@' : {
        	templateUrl: ''+ berkofs.ngtemplates +'/customerutility.html',
        	controller: 'CustomerController'
        },
        'gdrivecontent@customer.list' : {},
        'customercontent@' : {
        	templateUrl: ''+ berkofs.ngtemplates +'/customerlist.html',
        	controller: 'CustomerController'
        }
        
      }
    })
    .state('customer.view', {
      url: '/customer/:customerid',
      views: {
        'rootfolders' : {
          templateUrl: ''+ berkofs.ngtemplates +'/directorylist.html'
        },
        'utility@' : {
          templateUrl: ''+ berkofs.ngtemplates +'/customerutility.html',
          controller: 'CustomerController'
        },
        'gdrivecontent@customer.list' : {},
        'customercontent@' : {
          templateUrl: ''+ berkofs.ngtemplates +'/customerlist.html',
          controller: 'CustomerController'
        }
        
      }
    });
  $urlRouterProvider.otherwise('/');
  $locationProvider.html5Mode({
    enabled: true
  });
}]);

// window.wp = window.wp || {};

// wp.api = wp.api || angular.module( 'wp.api', [ 'ngResource' ] )
 
// 	// API resource
// 	.factory( 'wpAPIResource', [ '$resource', function ( $resource ) {

// 		return $resource(
// 			berkofs.api + '/:param1/:param2/:param3/:param4/:param5/:param6/:param7/',
// 			{
// 				_wp_json_nonce: berkofs.nonce
// 			}
// 		);

// 	}]);
