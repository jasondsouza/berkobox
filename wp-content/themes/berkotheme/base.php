<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?> ng-app="berkofsapp" ng-cloak>
  <?php get_template_part('templates/head'); ?>
  <base href="/">
  <body <?php body_class(); ?>  ng-controller="DirectoryController">
    <?php
      do_action('get_header');
    ?>
    <nav class="navbar navbar-fixed-top" id="main-navbar" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header col-md-2">
          <a ui-sref="home" class="btn navbar-brand">Berkowitz Furniture</a>
        </div> <!-- end navbar-header -->
        <a ui-sref="customer" class="btn btn-default navbar-btn navbar-right  col-md-1">Customer Details</a>
        <form role="search" class="navbar-form navbar-right col-md-9">
          <div class="form-group">
            <input type="search" id="searchbar" class="form-control" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-danger">Search</button>
        </form>        
      </div> <!-- end container -->
    </nav>
    <div class="container-fluid">
      <nav class="navbar navbar-vertical navbar-fixed-top col-md-2" id="directory-navbar" role="navigation">
        <div ui-view="rootfolders"></div>
        
      </nav>
      <div class="contentpane container col-md-offset-2 col-md-10">
        <div ui-view="utility"></div>
        <div ui-view="gdrivecontent"></div>
        <div ui-view="customercontent"></div>
        <div class="container-fluid">
          <ul>
  <li ng-repeat="file in data.rootFolders">{{file.title}}</li>
</ul>
        </div>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      wp_footer();
    ?>
  </body>
</html>
