<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'berkobox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'str4nger');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'CU?q;k901C+N31lVrBpN2 JcX:H)6t0q^{sryifCgPP--cMt&F$i$;}Z42X{NSc|');
define('SECURE_AUTH_KEY',  ' E&S.~s@Cm;y1>w=edxZ,W:?n;:Tay2BTra;$/$,zP2|;:XqSfIrq/i96$2iB%][');
define('LOGGED_IN_KEY',    'kc.GC`7jh -a:~HhO8)xgkM%lH,Zb`tU[w9J&t*@h>.(i&z|r}fe6V9-GKC^*@Q3');
define('NONCE_KEY',        '19NYBm[h}qd#&N1QH)KI[&GC-3uTc|{Fam{1]U7Jq?c[|3Rm!.@UIn!gqRlyKQ%{');
define('AUTH_SALT',        'm>CD<(,.#k)CCMg$Hx*1B4Po>Ehyi{.eR9j)2Hm{3d]3|*WTDes?XX!VuMYO:[72');
define('SECURE_AUTH_SALT', '#X45^S}]o(g7a90ek($jOg1*;K6OMkd)pC|*)#U9>qQ?hCQ/&0k[cUP!jj{u&(wj');
define('LOGGED_IN_SALT',   'g$;I|6a=iU+Xy}mK;~nCRKb(=G+3tw>IQCscb[0YDDlju]-4C>ro-7)%EEl}tn{~');
define('NONCE_SALT',       'Wpw$Eu/g(b[oNs;I=r;N|oT_=Mbh~p,-#VO5_y+{O_HrCXUwg;qq9mF[t(jP)w2~');

/**#@-*/
/*
JWT AUTH SECRET KEYS
 */
// define('JWT_AUTH_SECRET_KEY', 'i1n7&K p-P6p;AOl^o6z?h`s]t?h#tHwhZW(TuGO>QsV=L]|:fz=tI#onvDHNeEc');
// define('JWT_AUTH_CORS_ENABLE', true);
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
